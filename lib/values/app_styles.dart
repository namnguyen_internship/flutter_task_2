import 'package:flutter/cupertino.dart';
import 'package:flutter_app/values/app_colors.dart';

class FontFamily {
  static const inter = 'Inter';
}

class AppStyle {
  static TextStyle home = const TextStyle(
    fontFamily: FontFamily.inter,
    fontSize: 20,
    fontWeight: FontWeight.w400,
    color: Appcolors.primary,
  );
  static TextStyle current = const TextStyle(
    fontFamily: FontFamily.inter,
    fontSize: 18,
    fontWeight: FontWeight.w500,
    color: Appcolors.primary,
  );
  static TextStyle title = const TextStyle(
    fontFamily: FontFamily.inter,
    fontSize: 11,
    fontWeight: FontWeight.w600,
    color: Appcolors.grey,
  );
  static TextStyle discription = const TextStyle(
    fontFamily: FontFamily.inter,
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: Appcolors.primary,
  );
  static TextStyle boder = const TextStyle(
    fontFamily: FontFamily.inter,
    fontSize: 13,
    fontWeight: FontWeight.w600,
    color: Appcolors.orange,
  );
  static TextStyle bottomappbar = const TextStyle(
    fontFamily: FontFamily.inter,
    fontSize: 12,
    fontWeight: FontWeight.w500,
    color: Appcolors.grey,
  );
}
