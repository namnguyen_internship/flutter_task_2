import 'package:flutter/cupertino.dart';

class Appcolors {
  static const Color yellow = Color(0xffFFDE4A);
  static const Color primary = Color(0xff041967);
  static const Color orange = Color(0xffCA8E03);
  static const Color backgroundorange = Color(0xffFFFFFF);
  static const Color grey = Color(0xff3B3B3B);
  static const Color background = Color(0xffE1E9F1);
  static const Color navigativeappbar = Color(0xff008DFF);
  static const Color frame = Color(0xffDAE7F3);
  static const Color border = Color(0xffD6DEED);
  static const Color lightgrey = Color(0xff8C8C8C);
  static const Color black = Color(0xff000000);
  static const Color red = Color(0xffEF3900);
}
