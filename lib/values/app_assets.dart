class AppAssets {
  static const String iconpath = 'assets/icons/';
  static const String menu = '${iconpath}menu.svg';
  static const String home = '${iconpath}home.svg';
  static const String next = '${iconpath}next.svg';
  static const String oders = '${iconpath}oders.svg';
  static const String messages = '${iconpath}messages.svg';
  static const String notifications = '${iconpath}notifications.svg';
  static const String profile = '${iconpath}profile.svg';
  static const String filter = '${iconpath}filter.svg';
  static const String back = '${iconpath}back.svg';
  static const String contact = '${iconpath}contact.svg';
  static const String file_text = '${iconpath}file_text.svg';
  static const String log_out = '${iconpath}log_out.svg';
  static const String notification = '${iconpath}notification.svg';
  static const String avatar = '${iconpath}avatar.png';
  static const String edit = '${iconpath}edit.svg';
  static const String notification_inform =
      '${iconpath}notification_inform.svg';
  static const String notification_news = '${iconpath}notification_news.svg';
  static const String archive = '${iconpath}archive.svg';
  static const String delete = '${iconpath}delete.svg';
}
