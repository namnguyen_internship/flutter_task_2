import 'package:flutter/material.dart';
import 'package:flutter_app/pages/home_page.dart';
import 'package:flutter_app/pages/notifications_page.dart';
import 'package:flutter_app/pages/order_page.dart';
import 'package:flutter_app/values/app_assets.dart';
import 'package:flutter_app/values/app_colors.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Myhome extends StatefulWidget {
  const Myhome({Key? key}) : super(key: key);

  @override
  State<Myhome> createState() => _MyhomeState();
}

class _MyhomeState extends State<Myhome> {
  List<Widget> pageList = <Widget>[
    const PageHome(),
    const PageOrder(),
    const PageOrder(),
    const PageNotification(),
    const PageOrder(),
  ];
  int selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pageList[selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        selectedFontSize: 13,
        items: [
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AppAssets.home,
              color: selectedIndex == 0
                  ? Appcolors.navigativeappbar
                  : Appcolors.grey,
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AppAssets.oders,
              color: selectedIndex == 1
                  ? Appcolors.navigativeappbar
                  : Appcolors.grey,
            ),
            label: 'Oders',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AppAssets.messages,
              color: selectedIndex == 2
                  ? Appcolors.navigativeappbar
                  : Appcolors.grey,
            ),
            label: 'Messages',
          ),
          BottomNavigationBarItem(
              icon: SvgPicture.asset(
                AppAssets.notifications,
                color: selectedIndex == 3
                    ? Appcolors.navigativeappbar
                    : Appcolors.grey,
              ),
              label: 'Notifications'),
          BottomNavigationBarItem(
              icon: SvgPicture.asset(
                AppAssets.profile,
                color: selectedIndex == 4
                    ? Appcolors.navigativeappbar
                    : Appcolors.grey,
              ),
              label: 'Profile')
        ],
        currentIndex: selectedIndex,
        selectedItemColor: Appcolors.navigativeappbar,
        onTap: _onItemTapped,
      ),
    );
  }
}
