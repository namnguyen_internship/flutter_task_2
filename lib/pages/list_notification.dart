import 'package:flutter/cupertino.dart';

class notification {
  final String title;
  final String message;
  final String icon;
  final String action;
  const notification({
    required this.title,
    required this.message,
    required this.icon,
    required this.action,
  });
}

class list_notification {
  static const notifications = <notification>{
    notification(
      icon: 'assets/icons/notification_inform.svg',
      title: 'Order Status',
      message:
          'They are long establish fact that reader be distri the readable content page.',
      action: '10/26/2021',
    ),
    notification(
      icon: 'assets/icons/notification_news.svg',
      title: 'Promotion',
      message:
          'They are long establish fact that reader be distri the readable content page.',
      action: '10/26/2021',
    ),
    notification(
      icon: 'assets/icons/notification_news.svg',
      title: 'Promotion',
      message:
          'They are long establish fact that reader be distri the readable content page.',
      action: ' 10/26/2021',
    ),
    notification(
      icon: 'assets/icons/notification_inform.svg',
      title: 'Order Status',
      message:
          'They are long establish fact that reader be distri the readable content page.',
      action: ' 10/26/2021',
    ),
    notification(
      icon: 'assets/icons/notification_inform.svg',
      title: 'Order Status',
      message:
          'They are long establish fact that reader be distri the readable content page.  ',
      action: '10/26/2021',
    ),
    notification(
      icon: 'assets/icons/notification_inform.svg',
      title: 'Order Status',
      message:
          'They are long establish fact that reader be distri the readable content page. ',
      action: '10/26/2021',
    ),
  };
}
