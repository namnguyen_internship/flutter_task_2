import 'package:flutter/material.dart';
import 'package:flutter_app/pages/Drawer_page.dart';
import 'package:flutter_app/pages/list_notification.dart';
import 'package:flutter_app/values/app_assets.dart';
import 'package:flutter_app/values/app_colors.dart';
import 'package:flutter_app/values/app_styles.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PageNotification extends StatefulWidget {
  const PageNotification({Key? key}) : super(key: key);

  @override
  State<PageNotification> createState() => _PageNotificationState();
}

class _PageNotificationState extends State<PageNotification> {
  List<notification> items = [];
  final GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    items = List.of(list_notification.notifications);
    super.initState();
  }

  removeData(index) {
    setState(() {
      items.removeAt(index);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: AppBar(
        backgroundColor: Appcolors.yellow,
        title: Container(
          margin: EdgeInsets.only(top: 40, bottom: 20),
          child: Text(
            'NOTIFICATIONS',
            style: AppStyle.home,
          ),
        ),
        centerTitle: true,
        leading: (Container(
          child: IconButton(
            padding: EdgeInsets.only(top: 20, right: 15),
            icon: SvgPicture.asset(AppAssets.menu),
            onPressed: () {
              _scaffoldkey.currentState?.openDrawer();
            },
          ),
        )),
      ),
      drawer: const NavigationDrawer(),
      body: Container(
        margin: EdgeInsets.only(top: 4),
        child: ListView.builder(
          itemCount: items.length,
          itemBuilder: (context, index) {
            final item = items[index];
            return Column(
              children: [
                Container(
                  height: 114,
                  width: 382,
                  margin: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
                  child: Slidable(
                    key: ValueKey(index),
                    child: buildListTitle(item),
                    startActionPane:
                        ActionPane(motion: DrawerMotion(), children: [
                      SlidableAction(
                        onPressed: ((context) {
                          removeData(index);
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              duration: Duration(seconds: 1),
                              backgroundColor: Colors.white,
                              content: Container(
                                height: 66,
                                width: 390,
                                child: Container(
                                  height: 22,
                                  width: 358,
                                  margin: EdgeInsets.all(16),
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Column(
                                          children: [
                                            Row(
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      right: 14.6),
                                                  width: 28,
                                                  height: 28,
                                                  child: SvgPicture.asset(
                                                    AppAssets.archive,
                                                    color: Appcolors
                                                        .navigativeappbar,
                                                  ),
                                                ),
                                                Text(
                                                  'Notification archived',
                                                  style: AppStyle.discription
                                                      .copyWith(
                                                          fontSize: 14,
                                                          color:
                                                              Appcolors.black),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                        Container(
                                            child: FlatButton(
                                          padding: EdgeInsets.only(left: 50),
                                          onPressed: () {
                                            setState(() {
                                              items.insert(index, item);
                                            });
                                          },
                                          child: Text(
                                            'Undo',
                                            style: AppStyle.discription
                                                .copyWith(
                                                    color: Appcolors
                                                        .navigativeappbar),
                                          ),
                                        )),
                                      ]),
                                ),
                                decoration: BoxDecoration(
                                  color: Appcolors.backgroundorange,
                                  borderRadius: BorderRadius.circular(12),
                                  border: Border.all(
                                    color: Appcolors.background,
                                    width: 3,
                                  ),
                                ),
                              ),
                            ),
                          );
                        }),
                        backgroundColor: Appcolors.navigativeappbar,
                        foregroundColor: Colors.white,
                        icon: Icons.archive_outlined,
                        label: 'Archive',
                      ),
                    ]),
                    endActionPane:
                        ActionPane(motion: StretchMotion(), children: [
                      SlidableAction(
                        onPressed: ((context) {
                          removeData(index);
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              duration: Duration(seconds: 1),
                              backgroundColor: Colors.white,
                              content: Container(
                                height: 66,
                                width: 390,
                                child: Container(
                                  height: 22,
                                  width: 358,
                                  margin: EdgeInsets.all(16),
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Column(
                                          children: [
                                            Row(
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      right: 14.6),
                                                  width: 28,
                                                  height: 28,
                                                  child: SvgPicture.asset(
                                                    AppAssets.delete,
                                                    color: Appcolors.red,
                                                  ),
                                                ),
                                                Text(
                                                  'Notification deleted',
                                                  style: AppStyle.discription
                                                      .copyWith(
                                                          fontSize: 14,
                                                          color:
                                                              Appcolors.black),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                        Container(
                                            child: FlatButton(
                                          padding: EdgeInsets.only(left: 50),
                                          onPressed: () {
                                            setState(() {
                                              items.insert(index, item);
                                            });
                                          },
                                          child: Text(
                                            'Undo',
                                            style: AppStyle.discription
                                                .copyWith(
                                                    color: Appcolors
                                                        .navigativeappbar),
                                          ),
                                        )),
                                      ]),
                                ),
                                decoration: BoxDecoration(
                                  color: Appcolors.backgroundorange,
                                  borderRadius: BorderRadius.circular(12),
                                  border: Border.all(
                                    color: Appcolors.background,
                                    width: 3,
                                  ),
                                ),
                              ),
                            ),
                          );
                        }),
                        backgroundColor: Appcolors.red,
                        foregroundColor: Colors.white,
                        icon: Icons.delete_outline_outlined,
                        label: 'Delete',
                      ),
                    ]),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(
                      color: Appcolors.border,
                      width: 1,
                    ),
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

Widget buildListTitle(notification item) => Container(
      height: 92,
      width: 368,
      margin: EdgeInsets.only(left: item.title == 'Order Status' ? 14 : 0),
      child: item.title == 'Promotion'
          ? Row(
              children: [
                Container(
                  width: 14,
                  height: 92,
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 8, top: 12),
                        width: 6,
                        height: 6,
                        decoration: BoxDecoration(
                          color: Appcolors.red,
                          borderRadius: BorderRadius.circular(25),
                        ),
                      ),
                    ],
                  ),
                ),
                Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 8),
                      height: 28,
                      width: 346,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      child: Container(
                                          margin: EdgeInsets.only(
                                              left: 7.6,
                                              right: 7.07,
                                              top: 7.2,
                                              bottom: 7.47),
                                          child: SvgPicture.asset(
                                            item.icon,
                                            height: 13,
                                            width: 13,
                                          )),
                                      margin: EdgeInsets.only(right: 12),
                                      width: 28,
                                      height: 28,
                                      decoration: BoxDecoration(
                                        color: Appcolors.navigativeappbar,
                                        borderRadius: BorderRadius.circular(50),
                                      ),
                                    ),
                                    Text(
                                      item.title,
                                      style: AppStyle.discription,
                                    )
                                  ],
                                ),
                              ],
                            ),
                            Text(
                              item.action,
                              style: AppStyle.discription.copyWith(
                                  fontSize: 14, color: Appcolors.black),
                            ),
                          ]),
                    ),
                    Container(
                      height: 56,
                      width: 346,
                      child: Text(
                        item.message,
                        style: AppStyle.home.copyWith(
                            fontSize: 15.2,
                            color: Appcolors.black,
                            height: 1.9),
                      ),
                    ),
                  ],
                ),
              ],
            )
          : Column(
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: 8),
                  height: 28,
                  width: item.title == 'Order Status' ? 368 : 346,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            Row(
                              children: [
                                Container(
                                  child: Container(
                                      margin: EdgeInsets.only(
                                          left: 7.6,
                                          right: 7.07,
                                          top: 7.2,
                                          bottom: 7.47),
                                      child: SvgPicture.asset(
                                        item.icon,
                                        height: 13,
                                        width: 13,
                                      )),
                                  margin: EdgeInsets.only(right: 12),
                                  width: 28,
                                  height: 28,
                                  decoration: BoxDecoration(
                                    color: Appcolors.orange,
                                    borderRadius: BorderRadius.circular(50),
                                  ),
                                ),
                                Text(
                                  item.title,
                                  style: AppStyle.discription,
                                )
                              ],
                            ),
                          ],
                        ),
                        Text(
                          item.action,
                          style: AppStyle.discription
                              .copyWith(fontSize: 14, color: Appcolors.black),
                        ),
                      ]),
                ),
                Container(
                  height: 56,
                  width: 368,
                  child: Text(
                    item.message,
                    style: AppStyle.home.copyWith(
                        fontSize: 15.2, color: Appcolors.black, height: 1.9),
                  ),
                ),
              ],
            ),
    );
