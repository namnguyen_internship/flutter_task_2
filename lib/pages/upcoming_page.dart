import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_app/values/app_assets.dart';
import 'package:flutter_app/values/app_colors.dart';
import 'package:flutter_app/values/app_styles.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PageUpcoming extends StatefulWidget {
  const PageUpcoming({Key? key}) : super(key: key);

  @override
  State<PageUpcoming> createState() => _PageUpcomingState();
}

class _PageUpcomingState extends State<PageUpcoming> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Appcolors.yellow,
        title: Container(
          margin: EdgeInsets.only(top: 40, bottom: 20),
          child: Text(
            'UPCOMING ORDERS',
            style: AppStyle.home,
          ),
        ),
        centerTitle: true,
        leading: (Container(
          child: IconButton(
            padding: EdgeInsets.only(top: 20, right: 15),
            icon: SvgPicture.asset(AppAssets.back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        )),
        actions: [
          Container(
            child: IconButton(
              padding: EdgeInsets.only(top: 20),
              icon: SvgPicture.asset(AppAssets.filter),
              onPressed: () {
                print('hello');
              },
            ),
          ),
        ],
      ),
      body: Column(children: [
        Container(
          height: 176,
          width: 382,
          margin: EdgeInsets.only(top: 14, bottom: 12, left: 16, right: 16),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 16, top: 16),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 100),
                              child:
                                  Text('Order Placed', style: AppStyle.title),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 5),
                              child: Text('Mon, 16 Oct 2022 8:00 AM',
                                  style: AppStyle.discription
                                      .copyWith(fontSize: 13)),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 16, top: 16),
                    child: Column(
                      children: [
                        Container(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 3.5),
                            child: Text(
                              'Order Placed',
                              style: AppStyle.boder.copyWith(
                                  color: Appcolors.primary, fontSize: 13),
                            ),
                          ),
                          height: 31,
                          width: 106,
                          decoration: BoxDecoration(
                            color: Appcolors.backgroundorange,
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(
                              color: Appcolors.primary,
                              width: 3,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  border: Border.all(
                    color: Appcolors.border,
                    width: 1,
                  ),
                ),
              ),
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 16),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 145, bottom: 2),
                              child: Text(
                                'Estimated For Delivery',
                                style: AppStyle.title,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 47),
                              child: Text(
                                'Mon, 10/16/2022 4:30 PM',
                                style:
                                    AppStyle.discription.copyWith(fontSize: 18),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(
                                  right: 180, bottom: 2, top: 12),
                              child: Text(
                                'Pharmacy Name',
                                style: AppStyle.title,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 148),
                              child: Text(
                                'Roosevelt Clinic',
                                style:
                                    AppStyle.discription.copyWith(fontSize: 16),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
          decoration: BoxDecoration(
            color: Appcolors.background,
            borderRadius: BorderRadius.circular(20),
            border: Border.all(
              color: Appcolors.frame,
              width: 3,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: 12, left: 16, right: 16),
          height: 170,
          width: 382,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 16, top: 16),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 100),
                              child:
                                  Text('Order Placed', style: AppStyle.title),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 5),
                              child: Text('Mon, 16 Oct 2022 8:00 AM',
                                  style: AppStyle.discription
                                      .copyWith(fontSize: 13)),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 16, top: 16),
                    child: Column(
                      children: [
                        Container(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 3.5),
                            child: Text(
                              'Out For Delivery',
                              style: AppStyle.boder,
                            ),
                          ),
                          height: 31,
                          width: 126,
                          decoration: BoxDecoration(
                            color: Appcolors.backgroundorange,
                            borderRadius: BorderRadius.circular(30),
                            border: Border.all(
                              color: Appcolors.orange,
                              width: 3,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(
                    color: Appcolors.border,
                    width: 1,
                  ),
                ),
              ),
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 16),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 145, bottom: 2),
                              child: Text(
                                'Estimated For Delivery',
                                style: AppStyle.title,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 47),
                              child: Text(
                                'Mon, 10/16/2022 4:30 PM',
                                style:
                                    AppStyle.discription.copyWith(fontSize: 18),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(
                                  right: 175, bottom: 2, top: 12),
                              child: Text(
                                'Delivery Address',
                                style: AppStyle.title,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              child: Text(
                                '777 Brockton Avenue, Abington MA',
                                style: AppStyle.discription,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
          decoration: BoxDecoration(
            color: Appcolors.background,
            borderRadius: BorderRadius.circular(20),
            border: Border.all(
              color: Appcolors.frame,
              width: 3,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: 12, left: 16, right: 16),
          height: 170,
          width: 382,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 16, top: 16),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 100),
                              child:
                                  Text('Order Placed', style: AppStyle.title),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 5),
                              child: Text('Mon, 16 Oct 2022 8:00 AM',
                                  style: AppStyle.discription
                                      .copyWith(fontSize: 13)),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 16, top: 16),
                    child: Column(
                      children: [
                        Container(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 3.5),
                            child: Text(
                              'Out For Delivery',
                              style: AppStyle.boder,
                            ),
                          ),
                          height: 31,
                          width: 126,
                          decoration: BoxDecoration(
                            color: Appcolors.backgroundorange,
                            borderRadius: BorderRadius.circular(30),
                            border: Border.all(
                              color: Appcolors.orange,
                              width: 3,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(
                    color: Appcolors.border,
                    width: 1,
                  ),
                ),
              ),
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 16),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 145, bottom: 2),
                              child: Text(
                                'Estimated For Delivery',
                                style: AppStyle.title,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 47),
                              child: Text(
                                'Mon, 10/16/2022 4:30 PM',
                                style:
                                    AppStyle.discription.copyWith(fontSize: 18),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(
                                  right: 175, bottom: 2, top: 12),
                              child: Text(
                                'Delivery Address',
                                style: AppStyle.title,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              child: Text(
                                '777 Brockton Avenue, Abington MA',
                                style: AppStyle.discription,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
          decoration: BoxDecoration(
            color: Appcolors.background,
            borderRadius: BorderRadius.circular(20),
            border: Border.all(
              color: Appcolors.frame,
              width: 3,
            ),
          ),
        ),
      ]),
    );
  }
}
