import 'package:flutter/material.dart';
import 'package:flutter_app/values/app_assets.dart';
import 'package:flutter_app/values/app_colors.dart';
import 'package:flutter_app/values/app_styles.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PageOrder extends StatefulWidget {
  const PageOrder({Key? key}) : super(key: key);

  @override
  State<PageOrder> createState() => _PageOrderState();
}

class _PageOrderState extends State<PageOrder> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Appcolors.yellow,
        title: Container(
          margin: const EdgeInsets.only(top: 40, bottom: 20),
          child: Text(
            'ORDERS',
            style: AppStyle.home,
          ),
        ),
        centerTitle: true,
        actions: [
          Container(
            child: IconButton(
              padding: const EdgeInsets.only(top: 20),
              icon: SvgPicture.asset(AppAssets.filter),
              onPressed: () {
                print('hello');
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          DefaultTabController(
              length: 2, // length of tabs
              initialIndex: 0,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 16),
                      child: TabBar(
                        labelColor: Appcolors.primary,
                        unselectedLabelColor: Appcolors.lightgrey,
                        indicatorColor: Appcolors.primary,
                        tabs: [
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  padding: const EdgeInsets.only(
                                      top: 20, bottom: 11),
                                  child: const Text(
                                    'UPCOMING',
                                    style: TextStyle(
                                        fontFamily: FontFamily.inter,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 18),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  padding: const EdgeInsets.only(
                                      top: 20, bottom: 11),
                                  child: const Text(
                                    'HISTORY',
                                    style: TextStyle(
                                        fontFamily: FontFamily.inter,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 18),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        margin: const EdgeInsets.symmetric(horizontal: 16),
                        height: 777, //height of TabBarView
                        decoration: const BoxDecoration(
                            border: Border(
                                top: BorderSide(
                                    color: Appcolors.border, width: 1))),
                        child: TabBarView(children: <Widget>[
                          Container(
                            child: Column(children: [
                              Container(
                                height: 176,
                                width: 382,
                                margin:
                                    const EdgeInsets.only(top: 28, bottom: 12),
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          padding: const EdgeInsets.only(
                                              left: 16, top: 16),
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.only(
                                                            right: 100),
                                                    child: Text('Order Placed',
                                                        style: AppStyle.title),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.only(
                                                            right: 5),
                                                    child: Text(
                                                        'Mon, 16 Oct 2022 8:00 AM',
                                                        style: AppStyle
                                                            .discription
                                                            .copyWith(
                                                                fontSize: 13)),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          padding: const EdgeInsets.only(
                                              right: 16, top: 16),
                                          child: Column(
                                            children: [
                                              Container(
                                                child: Container(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 8,
                                                      vertical: 3.5),
                                                  child: Text(
                                                    'Order Placed',
                                                    style: AppStyle.boder
                                                        .copyWith(
                                                            color: Appcolors
                                                                .primary,
                                                            fontSize: 13),
                                                  ),
                                                ),
                                                height: 31,
                                                width: 106,
                                                decoration: BoxDecoration(
                                                  color: Appcolors
                                                      .backgroundorange,
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                  border: Border.all(
                                                    color: Appcolors.primary,
                                                    width: 3,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      margin: const EdgeInsets.symmetric(
                                          horizontal: 16, vertical: 12),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(30),
                                        border: Border.all(
                                          color: Appcolors.border,
                                          width: 1,
                                        ),
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          padding:
                                              const EdgeInsets.only(left: 16),
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Container(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 145,
                                                            bottom: 2),
                                                    child: Text(
                                                      'Estimated For Delivery',
                                                      style: AppStyle.title,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 47),
                                                    child: Text(
                                                      'Mon, 10/16/2022 4:30 PM',
                                                      style: AppStyle
                                                          .discription
                                                          .copyWith(
                                                              fontSize: 18),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 180,
                                                            bottom: 2,
                                                            top: 12),
                                                    child: Text(
                                                      'Pharmacy Name',
                                                      style: AppStyle.title,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        right: 148),
                                                    child: Text(
                                                      'Roosevelt Clinic',
                                                      style: AppStyle
                                                          .discription
                                                          .copyWith(
                                                              fontSize: 16),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                decoration: BoxDecoration(
                                  color: Appcolors.background,
                                  borderRadius: BorderRadius.circular(20),
                                  border: Border.all(
                                    color: Appcolors.frame,
                                    width: 3,
                                  ),
                                ),
                              ),
                              Container(
                                height: 170,
                                width: 382,
                                margin: EdgeInsets.only(bottom: 12),
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.only(
                                              left: 16, top: 16),
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        right: 100),
                                                    child: Text('Order Placed',
                                                        style: AppStyle.title),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        right: 5),
                                                    child: Text(
                                                        'Mon, 16 Oct 2022 8:00 AM',
                                                        style: AppStyle
                                                            .discription
                                                            .copyWith(
                                                                fontSize: 13)),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                              right: 16, top: 16),
                                          child: Column(
                                            children: [
                                              Container(
                                                child: Container(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 8,
                                                      vertical: 3.5),
                                                  child: Text(
                                                    'Order Placed',
                                                    style: AppStyle.boder
                                                        .copyWith(
                                                            color: Appcolors
                                                                .primary,
                                                            fontSize: 13),
                                                  ),
                                                ),
                                                height: 31,
                                                width: 106,
                                                decoration: BoxDecoration(
                                                  color: Appcolors
                                                      .backgroundorange,
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                  border: Border.all(
                                                    color: Appcolors.primary,
                                                    width: 3,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 16, vertical: 12),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(30),
                                        border: Border.all(
                                          color: Appcolors.border,
                                          width: 1,
                                        ),
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          padding: EdgeInsets.only(left: 16),
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        right: 145, bottom: 2),
                                                    child: Text(
                                                      'Estimated For Delivery',
                                                      style: AppStyle.title,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        right: 47),
                                                    child: Text(
                                                      'Mon, 10/16/2022 4:30 PM',
                                                      style: AppStyle
                                                          .discription
                                                          .copyWith(
                                                              fontSize: 18),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        right: 175,
                                                        bottom: 2,
                                                        top: 12),
                                                    child: Text(
                                                      'Pharmacy Name',
                                                      style: AppStyle.title,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    child: Text(
                                                      '777 Brockton Avenue, Abington MA',
                                                      style: AppStyle
                                                          .discription
                                                          .copyWith(
                                                              fontSize: 16),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                decoration: BoxDecoration(
                                  color: Appcolors.background,
                                  borderRadius: BorderRadius.circular(20),
                                  border: Border.all(
                                    color: Appcolors.frame,
                                    width: 3,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(bottom: 12),
                                height: 170,
                                width: 382,
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.only(
                                              left: 16, top: 16),
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        right: 100),
                                                    child: Text('Order Placed',
                                                        style: AppStyle.title),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        right: 5),
                                                    child: Text(
                                                        'Mon, 16 Oct 2022 8:00 AM',
                                                        style: AppStyle
                                                            .discription
                                                            .copyWith(
                                                                fontSize: 13)),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                              right: 16, top: 16),
                                          child: Column(
                                            children: [
                                              Container(
                                                child: Container(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 8,
                                                      vertical: 3.5),
                                                  child: Text(
                                                    'Out For Delivery',
                                                    style: AppStyle.boder,
                                                  ),
                                                ),
                                                height: 31,
                                                width: 126,
                                                decoration: BoxDecoration(
                                                  color: Appcolors
                                                      .backgroundorange,
                                                  borderRadius:
                                                      BorderRadius.circular(30),
                                                  border: Border.all(
                                                    color: Appcolors.orange,
                                                    width: 3,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 16, vertical: 12),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        border: Border.all(
                                          color: Appcolors.border,
                                          width: 1,
                                        ),
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          padding: EdgeInsets.only(left: 16),
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        right: 145, bottom: 2),
                                                    child: Text(
                                                      'Estimated For Delivery',
                                                      style: AppStyle.title,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        right: 47),
                                                    child: Text(
                                                      'Mon, 10/16/2022 4:30 PM',
                                                      style: AppStyle
                                                          .discription
                                                          .copyWith(
                                                              fontSize: 18),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        right: 175,
                                                        bottom: 2,
                                                        top: 12),
                                                    child: Text(
                                                      'Delivery Address',
                                                      style: AppStyle.title,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    child: Text(
                                                      '777 Brockton Avenue, Abington MA',
                                                      style:
                                                          AppStyle.discription,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                decoration: BoxDecoration(
                                  color: Appcolors.background,
                                  borderRadius: BorderRadius.circular(20),
                                  border: Border.all(
                                    color: Appcolors.frame,
                                    width: 3,
                                  ),
                                ),
                              ),
                              Container(
                                height: 170,
                                width: 382,
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.only(
                                              left: 16, top: 16),
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        right: 100),
                                                    child: Text('Order Placed',
                                                        style: AppStyle.title),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        right: 5),
                                                    child: Text(
                                                        'Mon, 16 Oct 2022 8:00 AM',
                                                        style: AppStyle
                                                            .discription
                                                            .copyWith(
                                                                fontSize: 13)),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                              right: 16, top: 16),
                                          child: Column(
                                            children: [
                                              Container(
                                                child: Container(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 8,
                                                      vertical: 3.5),
                                                  child: Text(
                                                    'Out For Delivery',
                                                    style: AppStyle.boder,
                                                  ),
                                                ),
                                                height: 31,
                                                width: 126,
                                                decoration: BoxDecoration(
                                                  color: Appcolors
                                                      .backgroundorange,
                                                  borderRadius:
                                                      BorderRadius.circular(30),
                                                  border: Border.all(
                                                    color: Appcolors.orange,
                                                    width: 3,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 16, vertical: 12),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        border: Border.all(
                                          color: Appcolors.border,
                                          width: 1,
                                        ),
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          padding: EdgeInsets.only(left: 16),
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        right: 145, bottom: 2),
                                                    child: Text(
                                                      'Estimated For Delivery',
                                                      style: AppStyle.title,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        right: 47),
                                                    child: Text(
                                                      'Mon, 10/16/2022 4:30 PM',
                                                      style: AppStyle
                                                          .discription
                                                          .copyWith(
                                                              fontSize: 18),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        right: 175,
                                                        bottom: 2,
                                                        top: 12),
                                                    child: Text(
                                                      'Delivery Address',
                                                      style: AppStyle.title,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        right: 148),
                                                    child: Text(
                                                      'Roosevelt Clinic',
                                                      style:
                                                          AppStyle.discription,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                decoration: BoxDecoration(
                                  color: Appcolors.background,
                                  borderRadius: BorderRadius.circular(20),
                                  border: Border.all(
                                    color: Appcolors.frame,
                                    width: 3,
                                  ),
                                ),
                              ),
                            ]),
                          ),
                          Container(
                            child: Center(
                              child: Text('Display Tab 2',
                                  style: TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.bold)),
                            ),
                          ),
                        ]))
                  ])),
        ]),
      ),
    );
  }
}
